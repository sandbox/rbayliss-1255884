<?php


function entity_token_filter_browser($form, &$form_state) {
  $defaults = array(
    'type' => 'node',
    'conditions' => array(),
  );
  $values = is_array($form_state['values']) ? array_intersect_key($form_state['values'], $defaults) + $defaults : $defaults;
  $types = array();
  $token_info = token_info();

  foreach(entity_get_info() as $machine_name => $entity_data) {
    if(isset($token_info['types'][$entity_data['token type']])) {
      $types[$machine_name] = $entity_data['label'];
    }
  }
  $form['criteria'] = array(
    '#type' => 'fieldset',
    '#id' => drupal_html_class(__FUNCTION__ . '-ajax-wrapper'),
  );
  $form['criteria']['type'] = array(
    '#title' => t('Entity Type'),
    '#type' => 'select',
    '#options' => $types,
    '#default_value' => $values['type'],
  );
  $form['criteria']['conditions'] = array(
    '#tree' => TRUE,
  );

  $idx = 0;
  foreach($values['conditions'] as $condition) {
    if(!empty($condition['value'])) {
      $form['criteria']['conditions'][$idx] = entity_token_filter_browser_condition_form($values['type'], $condition);
      $idx++;
    }
  }
  if($idx == 0) //Remove this to work on multivalue.
  $form['criteria']['conditions'][$idx] = entity_token_filter_browser_condition_form($values['type']);

  $form['criteria']['actions']['submit'] = array(
    '#value' => 'Save',
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'entity_token_filter_browser_ajax_submit',
      'wrapper' => 'entity-token-filter-browser-results',
    )
  );
  $form['results'] = array(
    '#type' => 'fieldset',
    '#id' => 'entity-token-filter-browser-results',
  );
  if(isset($form_state['execute_query']) && $form_state['execute_query']) {
    $form['results']['results'] = entity_token_filter_browser_results($values);
  }
  
  $form['#values'] = $values;
  return $form;
}

function entity_token_filter_browser_ajax_submit($form, &$form_state) {
  return $form['results'];
}

function entity_token_filter_browser_condition_form($type, $condition = array()) {
  $fields = array(
    'entity:entity_id' => 'ID',
    'entity:bundle' => 'Bundle',
  );
  $entity_info = entity_get_info($type);

  if(isset($entity_info['entity keys']['label'])) {
    $fields['property:' . $entity_info['entity keys']['label']] = t('Title');
    if(!isset($condition['field'])) {
      $condition['field'] = 'property:' . $entity_info['entity keys']['label'];
    }
  }

  $condition+= array(
    'field' => 'entity:entity_id',
    'operators' => 'LIKE',
    'value' => NULL,
  );
  $form['field'] = array(
    '#type' => 'select',
    '#options' => $fields,
    '#default_value' => $condition['field'],
  );
  $operators = array(
    'LIKE' => t('Like'),
    '=' => t('Equals'),
    '>' => t('Greater than'),
    '<' => t('Less Than'),
  );
  $form['operator'] = array(
    '#type' => 'select',
    '#options' => $operators,
    '#default_value' => $condition['field'],
  );
  $form['value'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $condition['value'],
  );
  return $form;
}

function entity_token_filter_browser_submit(&$form, &$form_state) {
  $form_state['execute_query'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function entity_token_filter_browser_results($values) {
  $type = $values['type'];
  $rows = $header = array();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $type);

  foreach($values['conditions'] as $condition) {
    if(!empty($condition['value'])) {
      list($prop_type, $field) = explode(':', $condition['field']);
      $method = $prop_type . 'Condition';
      $query->$method($field, $condition['value'], $condition['operator']);
    }
  }

  $query->entityOrderBy('entity_id', 'DESC');
  $query->pager();

  $result = $query->execute();

  if(!empty($result)) {
    $entities = entity_load($type, array_keys($result[$type]));

    $token_info = token_info();
    $tokens_of_type = $token_info['tokens'][$type];

    foreach($entities as $id => $entity) {
      $entity_tokens = array();

      foreach($tokens_of_type as $token_key => $description) {
        $entity_tokens[$token_key] = "[$type:$id:$token_key]";
      }

      $entity_tokens = token_generate($type, $entity_tokens, array($type => $entity));
      $entity_tokens = array_filter($entity_tokens);

      $items = array();
      foreach($entity_tokens as $token => $value) {
        $value = drupal_truncate_bytes($value, 150);
        $items[] = "$token - $value";
      }
      $rows[][]['data'] = array(
        '#theme' => 'item_list',
        '#items' => $items,
      );
    }
  }
  return array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
    '#empty' => 'There are no entities matching this query.',
  );
}